<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class phoneseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       for($i=1;$i<10;$i++){

            DB::table('phones')->insert([
            'name' => str::random(10),
            'company' => str::random(10),
            'price' => rand(3,5),
            ]);
        }
    }
}
