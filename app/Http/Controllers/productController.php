<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\product;
use App\Models\category;
use Session;

class productController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->session()->get('uid')!=""){
            $products=product::all();
            return view('product.index',compact('products'));            
        }    
        else{
            return redirect()->route('/')->with('loginstate','login first');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.addproduct');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'productName'=>'required',
            'category'=>'required',
            'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'price'=>'required',   
        ]);

        $fileName = time().'.'.$request->file('image')->extension();  
        $request->file('image')->move(public_path('products'), $fileName);
        
        category::find($request['category'])->products()->create(['productName' => $request['productName'],
                         'category_id' => $request['category'],
                         'Image' => $fileName,
                         'productPrice' => $request['price']
                     ]);


        return redirect()->route('product.index');
       
    }

    /**
     * Display the specified resource.
     *      
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=product::find($id);
        return view('product.editproduct',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        if($request->file('Image')){
            $fileName = time().'.'.$request->file('Image')->extension();  
            $request->file('Image')->move(public_path('products'), $fileName);
            product::find($id)->update(['productName'=>$request['productName'],
                                        'category_id'=>$request['category_id'],     
                                        'Image' => $fileName,
                                        'productPrice' => $request['productPrice'],
                                        ]);
        }
        else{
            product::find($id)->update($request->all());    
        }
          

        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        product::find($id)->delete();
        return redirect()->route('product.index');
    }
}
