<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\userController;
use Session;

class loginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register()
    {
        return view('login.register');
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validatedata(Request $request)
    {

        $email=$request['email'];
        $password=userController::encryption($request['password']);

        $data=User::where('email',$email)->where('password',$password)->get()->first();
        
        if(isset($data->id)){
            $request->session()->put('uid', $data->id);
            return redirect()->route('home');
        }
        return redirect()->route('/')->with('loginstate','login fail try again');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function redirectToHome(Request $request)
    {
        if($request->session()->get('uid')!="")
        {
            return view('home');
        }
        else
        {
            return redirect()->route('/');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        echo "destroy called";    
        $request->session()->forget('uid');
        return redirect()->route('/');
    }
}
