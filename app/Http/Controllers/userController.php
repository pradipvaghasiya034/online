<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->session()->get('uid')!=""){
            $users=User::all();
            return view('user.index',compact('users'));            
        }    
        else{
            return redirect()->route('/')->with('loginstate','login first');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('login.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //echo "<pre>";
        //print_r($request->all());die();
        $encryption=$this->encryption($request['password']);
        if($request->file('image')){

            $filename=time().'.'.$request->file('image')->extension();
            $request->file('image')->move(public_path('users'),$filename);
            User::create(['name'=>$request['name'],'email'=>$request['email'],'image'=>$filename,'password'=>$encryption]);
        }
        else{
            User::create(['name'=>$request['name'],'email'=>$request['email'],'image'=>'null','password'=>$encryption]);   
        }
        return redirect()->route('user.index');
    }

    public static function encryption($text)
    {
        $ciphering = "AES-128-CTR";

        $iv_length = openssl_cipher_iv_length($ciphering);
        $options = 0;

        // Non-NULL Initialization Vector for encryption
        $encryption_iv = '1234567891011121';

        // Store the encryption key
        $encryption_key = "a1b2c3d4e5f6g7h8";


        $encryption = openssl_encrypt($text, $ciphering,$encryption_key, $options, $encryption_iv);

        return $encryption;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function messagetype(Request $request)
    {
        $id=$request['id'];
        return view('user.message',compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=user::find($id);
        return view('user.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$encryption=$this->encryption($request['password']);
        if($request->file('image')){

            $filename=time().'.'.$request->file('image')->extension();
            $request->file('image')->move(public_path('users'),$filename);

            User::find($id)->update(['name'=>$request['name'],'email'=>$request['email'],'image'=>$filename]);
        }
        else{
            User::find($id)->update(['name'=>$request['name'],'email'=>$request['email']]);   
        }
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('user.index');
    }
}
