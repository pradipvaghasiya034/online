<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class product extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable=['productName','category_id','productPrice','Image'];

    public function category(){
        return $this->belongsTo('App\Models\category','id','category_id');
    }
}
