<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\product;

class category extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable=['categoryName','status'];

    public function products()
    {
        return $this->hasMany('App\Models\product','category_id','id');
    }
}
