<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\categoryController;
use App\Http\Controllers\productController;
use App\Http\Controllers\loginController;
use App\Http\Controllers\userController;
use App\Http\Controllers\messageController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//login controller route
Route::get('/',[loginController::class,'index'])->name('/');
Route::post('validate',[loginController::class,'validatedata'])->name('validate');
Route::get('destroy',[loginController::class,'destroy'])->name('destroy');
Route::get('/home',[loginController::class,'redirectToHome'])->name('home');

Route::get('messagetype/{id}',[userController::class,'messagetype'])->name('messagetype');


Route::resource('category',categoryController::class);
Route::resource('product',productController::class);
Route::resource('user',userController::class);
Route::resource('login',loginController::class);
Route::resource('message',messageController::class);

