@extends('layout.mainlayout');


@section('title')

@stop

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Edit Product</a></li>
@stop

@section('extra')
	<div></div>
@stop


@section('card-title')
	Edit Product
@stop

@section('card-content')

     @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
	<form action="{{ route('product.update',$data->id) }}" method="POST" enctype="multipart/form-data">
	@csrf          
    @method('PUT')
        <div class="form-group">
            <label>Product Name</label>
            <input type="text" name="productName" class="form-control" value="{{ $data->productName }}">
        </div>
        <div class="form-group">
            <label>category</label>
            <select name="category_id" class="form-control">
                @php 
                    $categorys=App\Models\category::all();
                @endphp
                @foreach($categorys as $category)
                    @if($category->id==$data->category_id)
                        <option value="{{ $category->id }}" selected>{{ $category->categoryName }}</option>
                    @else
                        <option value="{{ $category->id }}">{{ $category->categoryName }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <label for="formFile" class="form-label">Product Image</label><br>
            <input type="file" id="img_prd" name="Image"><br>
            <img src="/products/{{$data->Image}}" class="ml-3 mt-3" id="preview-image" width="300px">
        </div>
        <div class="form-group">
            <label>Price</label>
            <input type="text" name="productPrice" class="form-control" value="{{ $data->productPrice }}">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success" name="submit" value="Update">
            <input type="Reset" class="btn btn-danger" value="Reset">
        </div>
    </form>
	
@stop