@extends('layout.mainlayout');


@section('title')

@stop

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Add PRoduct</a></li>
@stop

@section('extra')
	<div></div>
@stop


@section('card-title')
	Add New
@stop

@section('card-content')

     @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
	<form action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data">
	@csrf          
        <div class="form-group">
            <label>Product Name</label>
            <input type="text" name="productName" class="form-control">
        </div>
        <div class="form-group">
            <label>category</label>
            <select name="category" class="form-control">
                @php 
                    $categorys=App\Models\category::all();
                @endphp
                @foreach($categorys as $category)
                    <option value="{{ $category->id }}">{{ $category->categoryName }}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <label for="formFile" class="form-label">Product Image</label><br>
            <input class="" type="file" name="image" id="img_prd">
            <img   class="ml-3 mt-3" id="preview-image" width="300px">
        </div>
        <div class="form-group">
            <label>Price</label>
            <input type="text" name="price" class="form-control">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success" name="submit" value="Add">
            <input type="Reset" class="btn btn-danger" value="Reset">
        </div>
    </form>
	
@stop