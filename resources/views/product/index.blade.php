@extends('layout.mainlayout')


@section('title')

@stop

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Product</a></li>
@stop

@section('extra')
	<div><a href="{{ route('product.create') }}" class="btn btn-success">Add Product</a></div>
@stop


@section('card-title')
	Products
@stop

@section('card-content')
	
	<table class="table text-center">
		<thead>
			<tr>
				<td class="text-center">No</td>
				<td class="text-center">Name</td>
				<td class="text-center">Category</td>
				<td class="text-center">price</td>
				<td class="text-center">Image</td>
				<td class="text-center">Actions</td>
			</tr>
		</thead>
	@foreach($products as $product)
		<tr>
			<td class="text-center">{{ $product->id }}</td>
			<td class="text-center">{{ $product->productName }}</td>
			<td class="text-center">
				@php
					$category=App\Models\category::find($product->category_id);
					echo $category->categoryName;
				@endphp	
			</td>
			<td class="text-center">{{ $product->productPrice }}</td>
			<td class="text-center"><img src="/products/{{$product->Image}}" width="150px" height="150px"></td>
			<td class="text-center">
				<form action="{{ route('product.destroy',$product->id) }}" method="POST">
					@csrf
					<a href="{{ route('product.edit',$product->id) }}" class="btn btn-primary">Edit</a>
					@method('DELETE')
					<input type="submit" name="submit" value="Delete" class="btn btn-danger">
				</form>
			</td>
		</tr>
	@endforeach
	</table>
@stop