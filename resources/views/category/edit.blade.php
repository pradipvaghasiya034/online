@extends('layout.mainlayout');


@section('title')
Edit Category	
@stop

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Edit Category</a></li>
@stop

@section('extra')
	<div></div>
@stop


@section('card-title')
	Edit
@stop

@section('card-content')
	<form action="{{ route('category.update',$data->id) }}" method="POST">
	@csrf     
    @method('patch')     
        <div class="form-group">
        <label>Category Name</label>
        <input type="text" name="categoryName" class="form-control" value="{{$data->categoryName}}">
        </div>
        <div class="form-group">
        <label>Status</label>
        <select name="status" class="form-control">
            @if($data->status==0)
            <option value="0" selected>Deactive</option>
            @else
            <option value="1">Active</option>
            @endif
        </select>
        <div class="form-group">
        <input type="submit" class="btn btn-success" name="submit" value="Update">
        <input type="Reset" class="btn btn-danger" value="Reset">
        </div>
    </form>
	
@stop