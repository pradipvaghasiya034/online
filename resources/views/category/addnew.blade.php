@extends('layout.mainlayout');


@section('title')
Add New Category	
@stop

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Add Category</a></li>
@stop

@section('extra')
	<div></div>
@stop


@section('card-title')
	Add New
@stop

@section('card-content')
	<form action="{{ route('category.store') }}" method="POST">
	@csrf          
        <div class="form-group">
        <label>Category Name</label>
        <input type="text" name="categoryName" class="form-control">
        </div>
        <div class="form-group">
        <label>Status</label>
        <select name="status" class="form-control">
            <option value="0">Deactive</option>
            <option value="1">Active</option>
        </select>
        </div>
        <div class="form-group">
        <input type="submit" class="btn btn-success" name="submit" value="Add">
        <input type="Reset" class="btn btn-danger" value="Reset">
        </div>
    </form>
	
@stop