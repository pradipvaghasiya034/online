@extends('layout.mainlayout')


@section('title')
Category	
@stop

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Category</a></li>
@stop

@section('extra')
	<div><a href="{{ route('category.create') }}" class="btn btn-success">Add Category</a></div>
@stop


@section('card-title')
	Categorys
@stop

@section('card-content')
	
	<table class="table text-center">
		<thead>
			<tr>
				<td class="text-center">No</td>
				<td class="text-center">Category</td>
				<td class="text-center">state</td>
				<td class="text-center">Actions</td>
			</tr>
		</thead>
	@foreach($categories as $category)
		<tr>
			<td class="text-center">{{ $category->id }}</td>
			<td class="text-center">{{ $category->categoryName }}</td>
			<td class="text-center">
				@if($category->status==0)
					Deactive
				@else
					Active
				@endif
			</td>
			<td class="text-center">
				<form action="{{ route('category.destroy',$category->id) }}" method="POST">
					@csrf
					<a href="{{ route('category.edit',$category->id) }}" class="btn btn-primary">Edit</a>
					@method('DELETE')
					<input type="submit" name="submit" value="Delete" class="btn btn-danger">
				</form>
			</td>
		</tr>
	@endforeach
	</table>
@stop