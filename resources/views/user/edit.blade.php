@extends('layout.mainlayout');


@section('title')

@stop

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Edit User</a></li>
@stop

@section('extra')
	<div></div>
@stop


@section('card-title')
	Edit User
@stop

@section('card-content')

     @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
	<form action="{{ route('user.update',$data->id) }}" method="POST" enctype="multipart/form-data">
	@csrf          
    @method('PUT')
        <div class="form-group">
            <label>User Name</label>
            <input type="text" name="name" class="form-control" value="{{ $data->name }}">
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control" value="{{ $data->email }}">
        </div>
        <div class="mb-3">
            <label for="formFile" class="form-label">User Image</label><br>
            <input type="file" id="img_prd" name="image"><br>
            <img src="/users/{{$data->image}}" class="ml-3 mt-3" id="preview-image" width="300px">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success" name="submit" value="Update">
            <input type="Reset" class="btn btn-danger" value="Reset">
        </div>
    </form>
	
@stop