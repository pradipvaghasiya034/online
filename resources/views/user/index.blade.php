@extends('layout.mainlayout')


@section('title')

@stop

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">User</a></li>
@stop

@section('extra')
	<div><a href="{{ route('user.create') }}" class="btn btn-success">Add User</a></div>
@stop


@section('card-title')
	Products
@stop

@section('card-content')
	
	<table class="table text-center">
		<thead>
			<tr>
				<td class="text-center">No</td>
				<td class="text-center">Name</td>
				<td class="text-center">Email</td>
				<td class="text-center">Image</td>
				<td class="text-center">Message</td>
			</tr>
		</thead>
	@isset($users)	
		@foreach($users as $user)
			<tr>
				<td class="text-center">{{ $user->id }}</td>
				<td class="text-center">{{ $user->name }}</td>
				<td class="text-center">{{ $user->email }}</td>
				<td><img src="/users/{{$user->image}}" width="100px" height="100px"></td>
				<td><a href="{{ route('messagetype', $user->id) }}" class="btn btn-success">Message</a></td>
			</tr>
		@endforeach
	@endisset
	</table>
@stop