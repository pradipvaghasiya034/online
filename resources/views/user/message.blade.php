@extends('layout.mainlayout');


@section('title')

@stop

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Send Message</a></li>
@stop

@section('extra')
	<div></div>
@stop


@section('card-title')
	Message
@stop

@section('card-content')

     @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
	<form action="{{route('message.store')}}" method="POST" enctype="multipart/form-data">
	@csrf          
        
        <input type="hidden" name="s_id" value="{{$id}}">

        <div class="form-group">
            <label>category</label>
            <select name="r_id" class="form-control" required>
                @php 
                    $users=App\Models\User::all();
                @endphp
                @foreach($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>
        </div>
        
        <div class="form-group">
            <label>Your Message</label>
            <input type="text" name="message" class="form-control" placeholder="Enter Message">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success" name="submit" value="Send">
        </div>
    </form>
	
@stop